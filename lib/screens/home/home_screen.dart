import 'package:flutter/material.dart';
import 'package:zhihu_demo/constants/global.dart';

import 'home_recommand_screen.dart';
import 'home_thoughts_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  int currentIndex = 0;
  late TabController tabController;
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 3, vsync: this);
    tabController.addListener(() {
      if (currentIndex == tabController.index) {
        return;
      }
      setState(() {
        currentIndex = tabController.index;
      });
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double mediaWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.black45,
        centerTitle: true,
        elevation: 1,
        backgroundColor: GlobalConstants.barWhite,
        automaticallyImplyLeading: true,
        title: SizedBox(
          width: mediaWidth * 0.6,
          child: TabBar(
            controller: tabController,
            labelColor: Colors.black,
            unselectedLabelColor: Colors.black54,
            splashFactory: NoSplash.splashFactory,
            overlayColor: MaterialStateProperty.resolveWith<Color?>(
              (Set<MaterialState> states) {
                return states.contains(MaterialState.focused)
                    ? null
                    : Colors.transparent;
              },
            ),
            indicatorColor: GlobalConstants.primaryBlue,
            indicatorSize: TabBarIndicatorSize.label,
            tabs: tabs(['想法', '推荐', '热榜']),
          ),
        ),
        actions: actions(),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          const HomeThoughts(),
          const HomeRecommand(),
          Container(),
        ],
      ),
    );
  }

  List<Widget> tabs(List<String> labels) {
    List<Widget> res = [];
    for (var label in labels) {
      res.add(tab(label));
    }
    return res;
  }

  Widget tab(String text) {
    return Text(
      text,
      maxLines: 1,
      style: const TextStyle(
        fontSize: 16.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  List<Widget> actions() {
    if (currentIndex == 1) {
      return [notificationAction()];
    }
    return [searchAction(), notificationAction()];
  }

  IconButton searchAction() {
    return IconButton(
      icon: const Icon(
        Icons.search,
        color: Colors.black,
      ),
      padding: EdgeInsets.zero,
      onPressed: () {},
      splashRadius: 20.0,
      constraints: const BoxConstraints(),
    );
  }

  IconButton notificationAction() {
    return IconButton(
      icon: const Icon(
        Icons.notifications_none,
        color: Colors.black,
      ),
      padding: EdgeInsets.zero,
      splashRadius: 20.0,
      onPressed: () {},
    );
  }
}
