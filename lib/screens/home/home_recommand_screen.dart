import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:zhihu_demo/constants/global.dart';
import 'package:zhihu_demo/models/models.dart';

class HomeRecommand extends StatefulWidget {
  const HomeRecommand({Key? key}) : super(key: key);

  @override
  State<HomeRecommand> createState() => _HomeRecommandState();
}

class _HomeRecommandState extends State<HomeRecommand> {
  int currentIndex = 0;
  List<String> tabs = ['全站', '直播', '娱乐', '数码', '知识', '校园', '种草', '经济', '农业'];
  List<Recommand> items = [];
  bool isLast = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // 小标题栏
        Container(
          color: GlobalConstants.barWhite,
          height: 40,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: tabs
                .asMap()
                .map((index, label) => MapEntry(index, tab(index, label)))
                .values
                .toList(),
          ),
        ),
        // 搜索框
        Container(
          color: GlobalConstants.barWhite,
          height: 40,
          margin: const EdgeInsets.symmetric(
            horizontal: 20,
          ),
          child: const TextField(
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 5,
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                borderSide: BorderSide(
                  color: GlobalConstants.primaryBlue,
                ),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
              labelStyle: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.black54,
              ),
              labelText: '你们有哪些极简的人生建议',
              floatingLabelBehavior: FloatingLabelBehavior.never,
              suffixIcon: Icon(
                Icons.search,
                color: GlobalConstants.primaryBlue,
              ),
            ),
          ),
        ),
        // 内容区
        Expanded(
          child: ListView.custom(
            childrenDelegate: SliverChildBuilderDelegate(
              (context, index) {
                // 需要展示的数据还没拿到
                if (index > items.length - 1) {
                  if (index == items.length) {
                    if (!isLast) {
                      getData(index);
                      return const SizedBox(
                        height: 24,
                        width: 24,
                        child: CircularProgressIndicator(),
                      );
                    }
                  }
                  return null;
                }
                return RecommandCard(items[index]);
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget tab(int index, String label) {
    double mediaWidth = MediaQuery.of(context).size.width;
    return SizedBox(
      height: 15,
      width: mediaWidth * 0.13,
      child: TextButton(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.bold,
            color: currentIndex == index ? Colors.black : Colors.black54,
          ),
        ),
        onPressed: () {
          setState(() {
            currentIndex = index;
          });
        },
      ),
    );
  }

  Future<void> getData(int index) async {
    String jsonstr = await DefaultAssetBundle.of(context)
        .loadString("assets/json/home_recommand_data.json");
    Iterable jsonArray = json.decode(jsonstr);
    List<Recommand> thoughts =
        jsonArray.map((e) => Recommand.fromJson(e)).toList();
    if (mounted) {
      setState(() {
        if (items.isNotEmpty) {
          return;
        }
        items.addAll(thoughts);
        isLast = true;
      });
    }
  }
}

class RecommandCard extends StatelessWidget {
  final Recommand recommand;
  const RecommandCard(this.recommand, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: GlobalConstants.barWhite,
      margin: const EdgeInsets.symmetric(
        vertical: 4,
        horizontal: 20,
      ),
      child: Column(
        children: [
          // 标题
          Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.symmetric(horizontal: 4),
            child: Text(
              "${recommand.title}",
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          // 用户
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset("assets/icons/${recommand.aviator}"),
              ),
              Text(
                "${recommand.username}",
                style: const TextStyle(
                  fontSize: 12,
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          // 内容
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 4),
            child: recommand.type == "0" ? video() : article(),
          ),
        ],
      ),
    );
  }

  Widget video() {
    Video? video = recommand.video;
    return Column(
      children: [
        Image.asset("assets/images/${video?.cover}"),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 3,
              child: bottomText("${video?.play} 播放 · ${video?.like} 赞同"),
            ),
            const Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.centerRight,
                child: ImageIcon(
                  AssetImage("assets/icons/more_setting.png"),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget article() {
    Article? article = recommand.article;
    return Column(
      children: [
        Text(
          "${article?.content}",
          style: const TextStyle(
            color: Color.fromRGBO(102, 102, 102, 1),
            fontWeight: FontWeight.bold,
            fontSize: 12,
          ),
        ),
        Row(
          children: [
            Expanded(
              flex: 3,
              child:
                  bottomText("${article?.like} 赞同 · ${article?.comments} 评论"),
            ),
            const Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.centerRight,
                child: ImageIcon(
                  AssetImage("assets/icons/more_setting.png"),
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  Widget bottomText(String label) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Text(
        label,
        style: const TextStyle(
          color: Color.fromRGBO(153, 153, 153, 1),
          fontSize: 12,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
