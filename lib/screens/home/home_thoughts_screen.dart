import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:zhihu_demo/constants/global.dart';
import 'package:zhihu_demo/models/models.dart';

class HomeThoughts extends StatefulWidget {
  const HomeThoughts({Key? key}) : super(key: key);

  @override
  State<HomeThoughts> createState() => _HomeThoughtsState();
}

class _HomeThoughtsState extends State<HomeThoughts> {
  List<Thought> items = [];
  bool isLast = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: GlobalConstants.bodyWhite,
      margin: const EdgeInsets.all(8),
      child: MasonryGridView.custom(
        mainAxisSpacing: 8,
        crossAxisSpacing: 8,
        gridDelegate: const SliverSimpleGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        childrenDelegate: SliverChildBuilderDelegate(
          (context, index) {
            // 需要展示的数据还没拿到
            if (index > items.length - 1) {
              if (index == items.length) {
                if (!isLast) {
                  getData(index);
                  return const CircularProgressIndicator();
                } else {
                  return null;
                }
              }

              return null;
            }
            // 拿到了数据，直接展示
            return ThoughtCard(items[index]);
          },
        ),
      ),
    );
  }

  Future<void> getData(int index) async {
    String jsonstr = await DefaultAssetBundle.of(context)
        .loadString("assets/json/home_thoughts_data.json");
    Iterable jsonArray = json.decode(jsonstr);
    List<Thought> thoughts = jsonArray.map((e) => Thought.fromJson(e)).toList();
    if (mounted) {
      setState(() {
        if (items.isNotEmpty) {
          return;
        }
        items.addAll(thoughts);
        isLast = true;
      });
    }
  }
}

class ThoughtCard extends StatelessWidget {
  final Thought thought;
  const ThoughtCard(this.thought, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      decoration: const BoxDecoration(
        color: GlobalConstants.barWhite,
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Image.asset(
              "assets/images/${thought.image}",
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: Text(
              "${thought.title}",
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 14,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 2,
                child: Row(
                  children: [
                    Image.asset(
                      "assets/icons/${thought.aviator}",
                    ),
                    const SizedBox(
                      width: 2,
                    ),
                    Text(
                      "${thought.username}",
                      style: const TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.bold,
                        color: Colors.black54,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    const ImageIcon(
                      AssetImage("assets/icons/thumb.png"),
                    ),
                    Text(
                      "${thought.likes}",
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: Colors.black54,
                      ),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
