import 'package:flutter/material.dart';
import 'package:zhihu_demo/constants/global.dart';

import 'home/home_screen.dart';

class NavigateScreen extends StatefulWidget {
  const NavigateScreen({Key? key}) : super(key: key);

  @override
  State<NavigateScreen> createState() => _NavigateScreenState();
}

class _NavigateScreenState extends State<NavigateScreen> {
  // 所有页面
  final List<Widget> _screens = [
    const HomeScreen(),
    const Scaffold(),
    const Scaffold(),
    const Scaffold(),
  ];
  // 当前页面
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _screens[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        elevation: 1,
        currentIndex: _currentIndex,
        backgroundColor: GlobalConstants.barWhite,
        type: BottomNavigationBarType.fixed,
        selectedLabelStyle: const TextStyle(
          fontSize: 12,
          color: GlobalConstants.primaryBlue,
          fontWeight: FontWeight.bold,
        ),
        unselectedLabelStyle: const TextStyle(
          fontSize: 12,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
        onTap: (index) => setState(() {
          _currentIndex = index;
        }),
        items: getNavItems(),
      ),
    );
  }

  List<BottomNavigationBarItem> getNavItems() {
    List<Item> items = [
      Item('首页', 'home'),
      Item('关注', 'follow'),
      Item('会员', 'member'),
      Item('我的', 'mine'),
    ];
    return items
        .asMap()
        .map((index, item) => MapEntry(index, getItem(index, item)))
        .values
        .toList();
  }

  BottomNavigationBarItem getItem(int index, Item item) {
    bool isSelected = index == _currentIndex;
    return BottomNavigationBarItem(
      label: item.label,
      icon: ImageIcon(
        color: isSelected ? GlobalConstants.primaryBlue : Colors.black,
        AssetImage(
          "assets/icons/bottom_bar_${item.iconName}_${isSelected ? 'selected' : 'unselected'}.png",
        ),
      ),
    );
  }
}

class Item {
  final String label;
  final String iconName;

  Item(this.label, this.iconName);
}
