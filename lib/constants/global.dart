import 'package:flutter/material.dart';

class GlobalConstants {
  static const primaryBlue = Color.fromRGBO(1, 102, 255, 1);
  static const bodyWhite = Color.fromRGBO(246, 246, 246, 1);
  static const barWhite = Colors.white;
}
