class Recommand {
  String? title;
  String? aviator;
  String? username;
  String? type;
  Video? video;
  Article? article;

  Recommand.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    aviator = json['aviator'];
    username = json['username'];
    type = json['type'];
    video = json['video'] != null ? Video.fromJson(json['video']) : null;
    article =
        json['article'] != null ? Article.fromJson(json['article']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {
      'title': title,
      'aviator': aviator,
      'username': username,
      'type': type,
    };
    if (video != null) {
      data['video'] = video!.toJson();
    }
    if (article != null) {
      data['article'] = article!.toJson();
    }
    return data;
  }
}

class Video {
  String? cover;
  String? play;
  String? like;

  Video.fromJson(Map<String, dynamic> json) {
    cover = json['cover'];
    play = json['play'];
    like = json['like'];
  }

  Map<String, dynamic> toJson() {
    return {
      'cover': cover,
      'play': play,
      'like': like,
    };
  }
}

class Article {
  String? content;
  String? like;
  String? comments;

  Article.fromJson(Map<String, dynamic> json) {
    content = json['content'];
    like = json['like'];
    comments = json['comments'];
  }

  Map<String, dynamic> toJson() {
    return {
      'content': content,
      'like': like,
      'comments': comments,
    };
  }
}
