class Thought {
  String? image;
  String? title;
  String? aviator;
  String? username;
  int? likes;

  Thought({this.image, this.title, this.aviator, this.username, this.likes});

  Thought.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    title = json['title'];
    aviator = json['aviator'];
    username = json['username'];
    likes = json['likes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['image'] = image;
    data['title'] = title;
    data['aviator'] = aviator;
    data['username'] = username;
    data['likes'] = likes;
    return data;
  }
}
